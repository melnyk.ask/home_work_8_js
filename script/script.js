// 1. Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
// DOM - це дерево, яке складається з тегів. Теги є об'єктами. Батьківські теги містять в собі
// "дитячі" теги. Кожний вкладений тег є батьком для своїх дітей-тегів.
// Теги є вузлами (елементами). Текст, який знахидиться всередині тегу є текстовим вузлом.

// 2. Какая разница между свойствами HTML-элементов innerHTML и innerText?
// Властивість innerHTML - містить елемент з тегами, наприклад < p > This is a paragraph</p >.
// Тоді, як властивість innerText містить лише текст без тегів "This is a paragraph".
// Обидві властивості можна змінити.

// 3. Как можно обратится к элементу страницы с помощью JS? Какой способ предпочтительнее?
// Для цього існують методи пошуку елементів з HTML-сторінки:
// getElementById - пошук по id елементу,
// querySelector, querySelectorAll - пошук елементів за селекторами CSS.
// Якщо багато елементів, які потрібно знайти за одним селекторм, краще використати
// querySelectorAll і далі застосовувати операції до всієй групи. Якщо елемент один - тоді
// використати querySelector.




// 1) Найти все параграфы на странице и установить цвет фона #ff0000
let p = document.querySelectorAll('p');

// for (let elem of p) { 
//     elem.style.backgroundColor = '#ff00ff';
// }

p.forEach(
    (elem) => { elem.style.backgroundColor = '#ff0000';}
);

// 2) Найти элемент с id="optionsList". Вывести в консоль. Найти родительский
// элемент и вывести в консоль.Найти дочерние ноды, если они есть, и вывести в
//консоль названия и тип нод.
let id_option_list = document.getElementById('optionsList');

console.log(id_option_list);
console.log(id_option_list.parentElement);
id_option_list.childNodes.forEach(
    (elem) => {
        console.log(
            `name of node: ${elem.nodeName}, type of node: ${elem.nodeType}`
        );
    }
);

// 3) Установите в качестве контента элемента с классом testParagraph
// следующий параграф < p > This is a paragraph</p >

// НЕТ "с классом testParagraph". Есть с id = "testParagraph"...
// По этому ищу по id.
let p_test_paragraph = document.querySelector('#testParagraph');
p_test_paragraph.innerHTML = '<p>This is a paragraph</p>';

// 4) Получить элементы <li>, вложенные в элемент с классом main-header и
// вывеcти их в консоль.Каждому из элементов присвоить новый класс nav-item.
let li = document.querySelectorAll('.main-header li');
li.forEach(
    (elem) => { 
        console.log(elem);
        elem.classList.add('nav-item');
    }
);

// 5) Найти все элементы с классом section-title. Удалить этот класс у элементов. 
let elements = document.querySelectorAll('.section-title');
elements.forEach(
    (elem) => { 
        elem.classList.remove('section-title');
    }
);
